package com.finartz.tdstester;

import com.finartz.tdstester.handler.LeftHandler;
import com.finartz.tdstester.handler.RightHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
@EnableWebFlux
public class Config {

    @Bean
    public RouterFunction<ServerResponse> route(WebClient client) {
        return RouterFunctions.route()
                .POST("/left", new LeftHandler(client))
                .POST("/right", new RightHandler())
                .build();
    }

    @Bean
    public WebClient client() {
        return WebClient.builder().build();
    }
}
