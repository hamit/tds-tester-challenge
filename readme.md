# Finartz 3D Secure Tester Challenge

Before you begin, clone this repository and then you may start it with the following command:

```bash
mvn spring-boot:run
```

## The Setup

Consider the following diagram:

![TesterChallenge](TesterChallenge.png)

Here:
 - The red arrows indicates a http request and blue arrows are http responses.
 - The order of messaging is indicated by numbers.
 - The messages are json. 
 - This project is a simulator tool for the ``Middle`` component. ``Middle`` acts like a proxy, passing and modifying 
 messages.
 - You are expected to test the ``Middle`` component, that means you have to simulate and 
 test both ``Left`` and ``Right`` components. That also means you are expected to write a 
 **client/server** test suite.
 
## The Use Case

We are going to run a simple messaging scenario among these components. Here ``Left`` and ``Right`` are you, and the 
``Middle`` is this project.

1. The ``left`` makes a http request to the _middle_ 
2. Before the ``left`` gets a response, _middle_ makes a request to ``right`` with a json message that is identical to 
the message sent by ``left`` but it adds its own id; ``middleId``.
3. The ``right`` responds to _middle_ still preserving the ``middleId`` value. (Do not delete it!)
4. Finally _middle_ responds to `left` the message it received from ``right``

## Test Case
1. As ``right``: test if you receive ``middleId`` value from _middle_.
2. As ``left``: test if ``middleId`` received and it has the same value as with ``right``

## About Json Messages
You may struct your own json message payload, however there are a couple constrains:
- The first request going to _middle_ must have ``url`` field. The value must hold the endpoint for ``right`` as _middle_
 is going to make the request to there.
```json
{
  "url": "http://rightServer:9090/",
  "mayKey": "mayVal"
}
 ```
- Do not delete ``middleId``, if you receive it you must pass it to next message.
```json
{
  "middleId": "a472e38d-4075-45f3-8602-efe64a2ef6b6",
  "mayKey": "mayVal"
}
``` 

## Technical Details
- You may use any programming language, framework, tool etc.
- The project must be uploaded to github with instructions about how to build and run.


